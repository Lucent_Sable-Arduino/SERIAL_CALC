/*
 *  uart.c
 *
 *  By S. J. Gosnell 
 *
 *  Demonstrates the UART receive transmission code
 *
 *  (C) 2013 The University of Waikato
 */

#include <string.h>
#include <stdio.h>
#include <lcd.h>
#include <serial.h>

char* strip(char* in, char* out)
{
	while(*in)
	{
		//if the current character is not in the set we want ignore it
		if((*in<'0'||*in>'9')
		    &&(*in!='+')
		    &&(*in!='-') 
		    &&(*in!='/') 
		    &&(*in!='*')
		    &&(*in!='\0'));
		else
			*(out++)=*in;
		in++; 
	}
	*out = 0;
	return out;
}

int calc(int a, int b, char op)
{
	switch(op)
	{
	case '+':
		return a+b;
		break;
	case '-':
		return a-b;
		break;
	case '*':
		return a*b;
		break;
	case '/':
		return a/b;
		break;
	}
}

void clear(char* str, int len)
{
	while(len)
	{
		str[len--]=0;
	}
}

int main(void)
{
	//make the line buffers one longer to allow
	//for a terminating 0
    //buffer for holding line 1
    char line[17];
    char stripped[17];

    serial_init();

    while(1)
    {
	int a = 0,b = 0,out = 0;
	char op = '+';
	//get a line from stdin (serial)
	clear(line,17);
	fgets(line,17,stdin);
	//remove the trailing newline if it exists
	strip(line,stripped);
	//find split the line into three parts
	sscanf(stripped,"%u%c%u",&a,&op,&b);
	out = calc(a,b,op);
	printf("%u%c%u=%d\n",a,op,b,out);
    }
    return 0;
}
